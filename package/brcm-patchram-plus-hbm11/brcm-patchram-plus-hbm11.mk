################################################################################
#
# brcm-patchram-plus-hbm11
#
################################################################################

BRCM_PATCHRAM_PLUS_HBM11_VERSION = v1.0
BRCM_PATCHRAM_PLUS_HBM11_SOURCE = brcm-patchram-plus-$(BRCM_PATCHRAM_PLUS_HBM11_VERSION).tar.gz
BRCM_PATCHRAM_PLUS_HBM11_SITE = https://bitbucket.org/lintech-berlin/brcm-patchram-plus/downloads
BRCM_PATCHRAM_PLUS_HBM11_LICENSE = Apache-2.0
BRCM_PATCHRAM_PLUS_HBM11_LICENSE_FILES = LICENSE.txt

define BRCM_PATCHRAM_PLUS_HBM11_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define BRCM_PATCHRAM_PLUS_HBM11_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(generic-package))
