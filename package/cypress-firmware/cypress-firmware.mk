################################################################################
#
# cypress-firmware
#
################################################################################

CYPRESS_FMAC_VERSION_MAJOR = v4.14.52
CYPRESS_FMAC_VERSION = $(CYPRESS_FMAC_VERSION_MAJOR)-2018_0928
CYPRESS_FMAC_DOWNLOAD_ID = 15932-1-39799
CYPRESS_FIRMWARE_SOURCE = cypress-fmac-$(CYPRESS_FMAC_VERSION).zip
CYPRESS_FIRMWARE_SITE = https://community.cypress.com/servlet/JiveServlet/download/$(CYPRESS_FMAC_DOWNLOAD_ID)
CYPRESS_FIRMWARE_VERSION = $(CYPRESS_FMAC_VERSION_MAJOR)-2018_0928
CYPRESS_FIRMWARE_LICENSE = Proprietary
CYPRESS_FIRMWARE_LICENSE_FILES = LICENCE

define CYPRESS_FIRMWARE_EXTRACT_CMDS
	$(UNZIP) -p $(DL_DIR)/cypress-firmware/$(CYPRESS_FIRMWARE_SOURCE) 'cypress-firmware-$(CYPRESS_FIRMWARE_VERSION).tar.gz' | \
	$(call suitable-extractor,$(@D)/cypress-firmware-$(CYPRESS_FIRMWARE_VERSION).tar.gz) - | \
	$(TAR) --strip-components=1 -C $(@D) $(TAR_OPTIONS) -
endef

ifeq ($(BR2_PACKAGE_CYPRESS_FIRMWARE_CYW4339),y)
CYPRESS_FIRMWARE_FILES += brcmfmac4339-sdio.bin
endif

ifeq ($(BR2_PACKAGE_CYPRESS_FIRMWARE_CYW43362),y)
CYPRESS_FIRMWARE_FILES += brcmfmac43362-sdio.bin
endif

ifeq ($(BR2_PACKAGE_CYPRESS_FIRMWARE_CYW43430),y)
CYPRESS_FIRMWARE_FILES += brcmfmac43430-sdio.bin brcmfmac43430-sdio.clm_blob
endif

ifneq ($(CYPRESS_FIRMWARE_FILES),)
define CYPRESS_FIRMWARE_INSTALL_FILES
	cd $(@D) && \
		$(TAR) cf install.tar $(sort $(CYPRESS_FIRMWARE_FILES)) && \
		$(TAR) xf install.tar -C $(TARGET_DIR)/lib/firmware/brcm
endef
endif

define CYPRESS_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware/brcm
	$(CYPRESS_FIRMWARE_INSTALL_FILES)
endef

$(eval $(generic-package))
