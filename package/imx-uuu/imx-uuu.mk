################################################################################
#
# imx-uuu
#
################################################################################

IMX_UUU_VERSION = 1.4.72
IMX_UUU_SOURCE = uuu_source-$(IMX_UUU_VERSION).tar.gz
IMX_UUU_SITE = https://github.com/NXPmicro/mfgtools/releases/download/uuu_$(IMX_UUU_VERSION)
IMX_UUU_LICENSE = BSD-3-Clause
IMX_UUU_LICENSE_FILES = LICENSE
HOST_IMX_UUU_DEPENDENCIES = host-libusb host-libzip host-openssl host-zlib

$(eval $(host-cmake-package))
