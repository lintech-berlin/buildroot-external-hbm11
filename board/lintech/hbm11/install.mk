install:
	install -m 0644 $(BINARIES_DIR)/boot.scr /srv/tftp
	install -m 0644 $(BINARIES_DIR)/SPL /srv/tftp
	install -m 0644 $(BINARIES_DIR)/SPL.nand /srv/tftp
	install -m 0644 $(BINARIES_DIR)/u-boot.img /srv/tftp
	install -m 0644 $(BINARIES_DIR)/rootfs.ubi /srv/tftp
