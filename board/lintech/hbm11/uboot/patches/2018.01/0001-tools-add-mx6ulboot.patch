From 75a852bb6a86bbbb658a287c890d3482b1ae07cd Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=B6rg=20Krause?= <joerg.krause@embedded.rocks>
Date: Wed, 23 Aug 2017 18:15:29 +0200
Subject: [PATCH 01/22] tools: add mx6ulboot
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Signed-off-by: Jörg Krause <joerg.krause@embedded.rocks>
---
 include/linux/bch.h |   3 +
 lib/bch.c           | 161 +++++++++++
 tools/Makefile      |   3 +
 tools/mx6ulboot.c   | 690 ++++++++++++++++++++++++++++++++++++++++++++
 4 files changed, 857 insertions(+)
 create mode 100644 tools/mx6ulboot.c

diff --git a/include/linux/bch.h b/include/linux/bch.h
index 28da402f46..9ba97d2b3b 100644
--- a/include/linux/bch.h
+++ b/include/linux/bch.h
@@ -65,4 +65,7 @@ int decode_bch(struct bch_control *bch, const uint8_t *data, unsigned int len,
 	       const uint8_t *recv_ecc, const uint8_t *calc_ecc,
 	       const unsigned int *syn, unsigned int *errloc);
 
+int encode_bch_ecc(void *source_block, size_t source_size,
+				   void *target_block, size_t target_size, int version);
+
 #endif /* _BCH_H */
diff --git a/lib/bch.c b/lib/bch.c
index d0d7e25c4e..d978d33160 100644
--- a/lib/bch.c
+++ b/lib/bch.c
@@ -208,6 +208,23 @@ static void store_ecc8(struct bch_control *bch, uint8_t *dst,
 	memcpy(dst, pad, BCH_ECC_BYTES(bch)-4*nwords);
 }
 
+/*
+ * reverse bit for byte
+ */
+static uint8_t reverse_bit(uint8_t in_byte)
+{
+	int i;
+	uint8_t out_byte = 0;
+
+	for (i = 0; i < 8; i++) {
+		if (in_byte & ((0x80) >> i)) {
+			out_byte |= 1 << i;
+		}
+	}
+
+	return out_byte;
+}
+
 /**
  * encode_bch - calculate BCH ecc parity of data
  * @bch:   BCH control structure
@@ -1399,3 +1416,147 @@ void free_bch(struct bch_control *bch)
 		kfree(bch);
 	}
 }
+
+int encode_bch_ecc(void *source_block, size_t source_size,
+				   void *target_block, size_t target_size,
+				   int version)
+{
+	struct bch_control *bch;
+	uint8_t *ecc_buf;
+	int ecc_buf_size;
+	uint8_t *tmp_buf;
+	int tmp_buf_size;
+	int real_buf_size;
+	int i, j;
+	int ecc_bit_off;
+	int data_ecc_blk_size;
+	int low_byte_off, low_bit_off;
+	int high_byte_off, high_bit_off;
+	uint8_t byte_low, byte_high;
+	uint8_t *psrc;
+
+	/* define the variables for bch algorithm*/
+	/* m:  METADATABYTE */
+	/* b0: BLOCK0BYTE */
+	/* e0: BLOCK0ECC */
+	/* bn: BLOCKNBYTE */
+	/* en: BLOCKNECC */
+	/* n : NUMOFBLOCKN */
+	/* gf: FCB_GF */
+	int m, b0, e0, bn, en, n, gf;
+
+	switch (version) {
+		/* 62 bit BCH, for i.MX6SX and i.MX7D */
+		case 2:
+			m  = 32;
+			b0 = 128;
+			e0 = 62;
+			bn = 128;
+			en = 62;
+			n  = 7;
+			gf = 13;
+			break;
+		/* 40 bit BCH, for i.MX6UL */
+		case 3:
+			m  = 32;
+			b0 = 128;
+			e0 = 40;
+			bn = 128;
+			en = 40;
+			n  = 7;
+			gf = 13;
+			break;
+		default:
+			printf("!!!!ERROR, bch version not defined\n");
+			return -EINVAL;
+			break;
+	}
+
+	/* sanity check */
+	/* nand data block must be large enough for FCB structure */
+	if (source_size > b0 + n * bn)
+		return -EINVAL;
+	/* nand page need to be large enough to contain Meta, FCB and ECC */
+	if (target_size < m + b0 + e0*gf/8 + n*bn + n*en*gf/8)
+		return -EINVAL;
+
+	/* init bch, using default polynomial */
+	bch = init_bch(gf, en, 0);
+	if(!bch)
+		return -EINVAL;
+
+	/* buffer for ecc */
+	ecc_buf_size = (gf * en + 7)/8;
+	ecc_buf = malloc(ecc_buf_size);
+	if(!ecc_buf)
+		return -EINVAL;
+
+	/* temp buffer to store data and ecc */
+	tmp_buf_size = b0 + (e0 * gf + 7)/8 + (bn + (en * gf + 7)/8) * 7;
+	tmp_buf = malloc(tmp_buf_size);
+	if(!tmp_buf)
+		return -EINVAL;
+	memset(tmp_buf, 0, tmp_buf_size);
+
+	/* generate ecc code for each data block and store in temp buffer */
+
+	for (i = 0; i < n+1; i++) {
+		memset(ecc_buf, 0, ecc_buf_size);
+		psrc = source_block + i * bn;
+
+		/*
+		 * imx-kobs uses a modified encode_bch which reverses the
+		 * bit order of the data before calculating bch.
+		 * Do this in the buffer and use the bch lib here.
+		 */
+		for (j = 0; j < bn; j++)
+			psrc[j] = reverse_bit(psrc[j]);
+
+		encode_bch(bch, psrc, bn, ecc_buf);
+
+		for (j = 0; j < bn; j++)
+			psrc[j] = reverse_bit(psrc[j]);
+
+		memcpy(tmp_buf + i * (bn + ecc_buf_size), source_block + i * bn, bn);
+
+		/* reverse ecc bit */
+		for (j = 0; j < ecc_buf_size; j++) {
+			ecc_buf[j] = reverse_bit(ecc_buf[j]);
+		}
+
+		memcpy(tmp_buf + (i+1)*bn + i*ecc_buf_size, ecc_buf, ecc_buf_size);
+	}
+
+	/* store Metadata for taget block with randomizer*/
+	/*memcpy(target_block, RandData, m);*/
+	memset(target_block, 0, m);
+
+	/* shift the bit to combine the source data and ecc */
+	real_buf_size = (b0*8 + gf*e0 + (bn*8 + gf*en)*n)/8;
+
+	if (!((gf * en)%8)) {
+		/* ecc data is byte aligned, just copy it. */
+		memcpy(target_block + m, tmp_buf, real_buf_size);
+	} else {
+		/* bit offset for each ecc block */
+		ecc_bit_off = 8 - (gf * en)%8;
+		/* size of a data block plus ecc block */
+		data_ecc_blk_size = bn +(gf*en+7)/8;
+
+		for (i = 0; i < real_buf_size; i++) {
+			low_bit_off = ((i/data_ecc_blk_size) * ecc_bit_off)%8;
+			low_byte_off = ((i/data_ecc_blk_size) * ecc_bit_off)/8;
+			high_bit_off = (((i+1)/data_ecc_blk_size) * ecc_bit_off)%8;
+			high_byte_off = (((i+1)/data_ecc_blk_size) * ecc_bit_off)/8;
+
+			byte_low = tmp_buf[i+low_byte_off] >> low_bit_off;
+			byte_high = tmp_buf[i+1+high_byte_off] << (8 - high_bit_off);
+
+			*(uint8_t *)(target_block + i + m) = (byte_low | byte_high);
+		}
+	}
+
+	free(ecc_buf);
+	free(tmp_buf);
+	return 0;
+}
diff --git a/tools/Makefile b/tools/Makefile
index 4d32fe5910..f1f806090b 100644
--- a/tools/Makefile
+++ b/tools/Makefile
@@ -170,6 +170,9 @@ hostprogs-$(CONFIG_MX23) += mxsboot
 hostprogs-$(CONFIG_MX28) += mxsboot
 HOSTCFLAGS_mxsboot.o := -pedantic
 
+hostprogs-$(CONFIG_MX6UL)$(CONFIG_MX6ULL) += mx6ulboot
+mx6ulboot-objs := mx6ulboot.o lib/bch.o
+
 hostprogs-$(CONFIG_ARCH_SUNXI) += mksunxiboot
 hostprogs-$(CONFIG_ARCH_SUNXI) += sunxi-spl-image-builder
 sunxi-spl-image-builder-objs := sunxi-spl-image-builder.o lib/bch.o
diff --git a/tools/mx6ulboot.c b/tools/mx6ulboot.c
new file mode 100644
index 0000000000..3c4489a337
--- /dev/null
+++ b/tools/mx6ulboot.c
@@ -0,0 +1,690 @@
+/*
+ * Freescale i.MX6UL image generator
+ *
+ * Based on mxsboot written by: Marek Vasut <marek.vasut@gmail.com>
+ *
+ * Copyright (C) 2017 Jörg Krause <joerg.krause@embedded.rocks>
+ *
+ * SPDX-License-Identifier:	GPL-2.0+
+ */
+
+#include <fcntl.h>
+#include <sys/stat.h>
+#include <sys/types.h>
+#include <unistd.h>
+#include <linux/bch.h>
+
+#include "compiler.h"
+#include "imximage.h"
+
+/* Taken from <linux/kernel.h> */
+#define __round_mask(x, y) ((__typeof__(x))((y)-1))
+#define round_down(x, y) ((x) & ~__round_mask(x, y))
+
+#define ALIGN(x, a) __ALIGN_MASK((x), (typeof(x))(a)-1)
+#define __ALIGN_MASK(x, mask) (((x)+(mask))&~(mask))
+
+/*
+ * Default BCB layout.
+ *
+ * TWEAK this if you have blown any OCOTP fuses.
+ */
+#define STRIDE_PAGES		64
+#define STRIDE_COUNT		4
+
+/*
+ * Layout for 256Mb big NAND with 2048b page size, 64b OOB size and
+ * 128kb erase size.
+ *
+ * TWEAK this if you have different kind of NAND chip.
+ */
+static unsigned nand_writesize = 2048;
+static unsigned nand_oobsize = 64;
+static unsigned nand_erasesize = 128 * 1024;
+
+/*
+ * Sector on which the SigmaTel boot partition (0x53) starts.
+ */
+static unsigned sd_sector = 2048;
+
+/*
+ * Each of the U-Boot bootstreams is at maximum 1MB big.
+ *
+ * TWEAK this if, for some wild reason, you need to boot bigger image.
+ */
+#define MAX_BOOTSTREAM_SIZE	(1 * 1024 * 1024)
+
+/* i.MX6UL NAND controller-specific constants. DO NOT TWEAK! */
+#define MXS_NAND_CHUNK_DATA_CHUNK_SIZE	512
+#define MXS_NAND_METADATA_SIZE		10
+#define MXS_NAND_BITS_PER_ECC_LEVEL	13
+
+struct mx6ul_nand_fcb {
+	uint32_t		checksum;
+	uint32_t		fingerprint;
+	uint32_t		version;
+	struct {
+		uint8_t			data_setup;
+		uint8_t			data_hold;
+		uint8_t			address_setup;
+		uint8_t			dsample_time;
+		uint8_t			nand_timing_state;
+		uint8_t			rea;
+		uint8_t			rloh;
+		uint8_t			rhoh;
+	}			timing;
+	uint32_t		page_data_size;
+	uint32_t		total_page_size;
+	uint32_t		sectors_per_block;
+	uint32_t		number_of_nands;		/* Ignored */
+	uint32_t		total_internal_die;		/* Ignored */
+	uint32_t		cell_type;			/* Ignored */
+	uint32_t		ecc_block_n_ecc_type;
+	uint32_t		ecc_block_0_size;
+	uint32_t		ecc_block_n_size;
+	uint32_t		ecc_block_0_ecc_type;
+	uint32_t		metadata_bytes;
+	uint32_t		num_ecc_blocks_per_page;
+	uint32_t		ecc_block_n_ecc_level_sdk;	/* Ignored */
+	uint32_t		ecc_block_0_size_sdk;		/* Ignored */
+	uint32_t		ecc_block_n_size_sdk;		/* Ignored */
+	uint32_t		ecc_block_0_ecc_level_sdk;	/* Ignored */
+	uint32_t		num_ecc_blocks_per_page_sdk;	/* Ignored */
+	uint32_t		metadata_bytes_sdk;		/* Ignored */
+	uint32_t		erase_threshold;		/* Ignored */
+	uint32_t		boot_patch;			/* Ignored */
+	uint32_t		patch_sectors;			/* Ignored */
+	uint32_t		firmware1_starting_page;
+	uint32_t		firmware2_starting_page;
+	uint32_t		pages_in_firmware1;
+	uint32_t		pages_in_firmware2;
+	uint32_t		dbbt_search_area_start_address;
+	uint32_t		badblock_marker_byte;
+	uint32_t		badblock_marker_start_bit;
+	uint32_t		bb_marker_physical_offset;
+	uint32_t		bch_type;
+};
+
+struct mx6ul_nand_dbbt {
+	uint32_t		checksum;
+	uint32_t		fingerprint;
+	uint32_t		version;
+	uint32_t		reserved;
+	uint32_t		dbbt_num_of_pages;
+};
+
+struct mx6ul_nand_bbt {
+	uint32_t		nand;
+	uint32_t		number_bb;
+	uint32_t		badblock[510];
+};
+
+struct mx6ul_sd_drive_info {
+	uint32_t		chip_num;
+	uint32_t		drive_type;
+	uint32_t		tag;
+	uint32_t		first_sector_number;
+	uint32_t		sector_count;
+};
+
+struct mx6ul_sd_config_block {
+	uint32_t			signature;
+	uint32_t			primary_boot_tag;
+	uint32_t			secondary_boot_tag;
+	uint32_t			num_copies;
+	struct mx6ul_sd_drive_info	drv_info[1];
+};
+
+static inline uint32_t mx6ul_nand_ecc_chunk_cnt(uint32_t page_data_size)
+{
+	return page_data_size / MXS_NAND_CHUNK_DATA_CHUNK_SIZE;
+}
+
+static inline uint32_t mx6ul_nand_ecc_size_in_bits(uint32_t ecc_strength)
+{
+	return ecc_strength * MXS_NAND_BITS_PER_ECC_LEVEL;
+}
+
+static inline uint32_t mx6ul_nand_get_ecc_strength(uint32_t page_data_size,
+						   uint32_t page_oob_size)
+{
+	int ecc_strength;
+
+	/*
+	 * Determine the ECC layout with the formula:
+	 *	ECC bits per chunk = (total page spare data bits) /
+	 *		(bits per ECC level) / (chunks per page)
+	 * where:
+	 *	total page spare data bits =
+	 *		(page oob size - meta data size) * (bits per byte)
+	 */
+	ecc_strength = ((page_oob_size - MXS_NAND_METADATA_SIZE) * 8)
+			/ (MXS_NAND_BITS_PER_ECC_LEVEL *
+			    mx6ul_nand_ecc_chunk_cnt(page_data_size));
+
+	return round_down(ecc_strength, 2);
+}
+
+static inline uint32_t mx6ul_nand_get_mark_offset(uint32_t page_data_size,
+						  uint32_t ecc_strength)
+{
+	uint32_t chunk_data_size_in_bits;
+	uint32_t chunk_ecc_size_in_bits;
+	uint32_t chunk_total_size_in_bits;
+	uint32_t block_mark_chunk_number;
+	uint32_t block_mark_chunk_bit_offset;
+	uint32_t block_mark_bit_offset;
+
+	chunk_data_size_in_bits = MXS_NAND_CHUNK_DATA_CHUNK_SIZE * 8;
+	chunk_ecc_size_in_bits  = mx6ul_nand_ecc_size_in_bits(ecc_strength);
+
+	chunk_total_size_in_bits =
+			chunk_data_size_in_bits + chunk_ecc_size_in_bits;
+
+	/* Compute the bit offset of the block mark within the physical page. */
+	block_mark_bit_offset = page_data_size * 8;
+
+	/* Subtract the metadata bits. */
+	block_mark_bit_offset -= MXS_NAND_METADATA_SIZE * 8;
+
+	/*
+	 * Compute the chunk number (starting at zero) in which the block mark
+	 * appears.
+	 */
+	block_mark_chunk_number =
+			block_mark_bit_offset / chunk_total_size_in_bits;
+
+	/*
+	 * Compute the bit offset of the block mark within its chunk, and
+	 * validate it.
+	 */
+	block_mark_chunk_bit_offset = block_mark_bit_offset -
+			(block_mark_chunk_number * chunk_total_size_in_bits);
+
+	if (block_mark_chunk_bit_offset > chunk_data_size_in_bits)
+		return 1;
+
+	/*
+	 * Now that we know the chunk number in which the block mark appears,
+	 * we can subtract all the ECC bits that appear before it.
+	 */
+	block_mark_bit_offset -=
+		block_mark_chunk_number * chunk_ecc_size_in_bits;
+
+	return block_mark_bit_offset;
+}
+
+static inline uint32_t mx6ul_nand_mark_byte_offset(void)
+{
+	uint32_t ecc_strength;
+	ecc_strength = mx6ul_nand_get_ecc_strength(nand_writesize,
+						   nand_oobsize);
+	return mx6ul_nand_get_mark_offset(nand_writesize, ecc_strength) >> 3;
+}
+
+static inline uint32_t mx6ul_nand_mark_bit_offset(void)
+{
+	uint32_t ecc_strength;
+	ecc_strength = mx6ul_nand_get_ecc_strength(nand_writesize,
+						   nand_oobsize);
+	return mx6ul_nand_get_mark_offset(nand_writesize, ecc_strength) & 0x7;
+}
+
+static uint32_t mx6ul_nand_block_csum(uint8_t *block, uint32_t size)
+{
+	uint32_t csum = 0;
+	int i;
+
+	for (i = 0; i < size; i++)
+		csum += block[i];
+
+	return csum ^ 0xffffffff;
+}
+
+#define FCB_FINGERPRINT		0x20424346	//!< 'FCB '
+#define FCB_VERSION_1		0x01000000
+static struct mx6ul_nand_fcb *mx6ul_nand_get_fcb(size_t fw_size_aligned)
+{
+	struct mx6ul_nand_fcb *fcb;
+	unsigned search_area_size_in_bytes;
+	unsigned stride_size_in_bytes;
+	unsigned boot_stream1_pos;
+	unsigned boot_stream2_pos;
+
+	fcb = malloc(nand_writesize);
+	if (!fcb) {
+		printf("MX6UL NAND: Unable to allocate FCB\n");
+		return NULL;
+	}
+
+	memset(fcb, 0, nand_writesize);
+
+	fcb->fingerprint =		FCB_FINGERPRINT;
+	fcb->version =			FCB_VERSION_1;
+
+	/*
+	 * FIXME: These here are default values as found in kobs-ng. We should
+	 * probably retrieve the data from NAND or something.
+	 */
+	fcb->timing.data_setup =	80;
+	fcb->timing.data_hold =		60;
+	fcb->timing.address_setup =	25;
+	fcb->timing.dsample_time =	6;
+
+	fcb->page_data_size =		nand_writesize;
+	fcb->total_page_size =		nand_writesize + nand_oobsize;
+	fcb->sectors_per_block =	nand_erasesize / nand_writesize;
+
+	fcb->num_ecc_blocks_per_page =
+			(nand_writesize / MXS_NAND_CHUNK_DATA_CHUNK_SIZE) - 1;
+	fcb->ecc_block_0_size =		MXS_NAND_CHUNK_DATA_CHUNK_SIZE;
+	fcb->ecc_block_n_size =		MXS_NAND_CHUNK_DATA_CHUNK_SIZE;
+	fcb->metadata_bytes =		MXS_NAND_METADATA_SIZE;
+
+	fcb->ecc_block_n_ecc_type = mx6ul_nand_get_ecc_strength(
+					nand_writesize, nand_oobsize) >> 1;
+	fcb->ecc_block_0_ecc_type = mx6ul_nand_get_ecc_strength(
+					nand_writesize, nand_oobsize) >> 1;
+	if (fcb->ecc_block_n_ecc_type == 0) {
+		printf("MX6UL NAND: Unsupported NAND geometry\n");
+		goto err;
+	}
+	fcb->badblock_marker_byte = mx6ul_nand_mark_byte_offset();
+	fcb->badblock_marker_start_bit = mx6ul_nand_mark_bit_offset();
+	fcb->bb_marker_physical_offset = nand_writesize;
+
+	/* rom boot settings */
+	stride_size_in_bytes = STRIDE_PAGES * nand_writesize;
+	search_area_size_in_bytes = stride_size_in_bytes * STRIDE_COUNT;
+
+	/* Compute the positions of the boot stream copies. */
+	boot_stream1_pos = 2 * search_area_size_in_bytes;
+	boot_stream2_pos = boot_stream1_pos + MAX_BOOTSTREAM_SIZE;
+
+	fcb->firmware1_starting_page = boot_stream1_pos / nand_writesize;
+	fcb->firmware2_starting_page = boot_stream2_pos / nand_writesize;
+	fcb->pages_in_firmware1 = fw_size_aligned / nand_writesize;
+	fcb->pages_in_firmware2 = fw_size_aligned / nand_writesize;
+
+	fcb->dbbt_search_area_start_address = search_area_size_in_bytes;
+
+	fcb->bch_type = 0;
+
+	return fcb;
+
+err:
+	free(fcb);
+	return NULL;
+}
+
+#define DBBT_FINGERPRINT2	0x54424244	//!< 'DBBT'
+#define DBBT_VERSION_1		0x01000000
+static struct mx6ul_nand_dbbt *mx6ul_nand_get_dbbt(void)
+{
+	struct mx6ul_nand_dbbt *dbbt;
+
+	dbbt = malloc(nand_writesize);
+	if (!dbbt) {
+		printf("MX6UL NAND: Unable to allocate DBBT\n");
+		return NULL;
+	}
+
+	memset(dbbt, 0, nand_writesize);
+
+	dbbt->fingerprint	= DBBT_FINGERPRINT2;
+	dbbt->version		= DBBT_VERSION_1;
+	dbbt->dbbt_num_of_pages = 1;
+
+	return dbbt;
+}
+
+static inline uint8_t mx6ul_nand_parity_13_8(const uint8_t b)
+{
+	uint32_t parity = 0, tmp;
+
+	tmp = ((b >> 6) ^ (b >> 5) ^ (b >> 3) ^ (b >> 2)) & 1;
+	parity |= tmp << 0;
+
+	tmp = ((b >> 7) ^ (b >> 5) ^ (b >> 4) ^ (b >> 2) ^ (b >> 1)) & 1;
+	parity |= tmp << 1;
+
+	tmp = ((b >> 7) ^ (b >> 6) ^ (b >> 5) ^ (b >> 1) ^ (b >> 0)) & 1;
+	parity |= tmp << 2;
+
+	tmp = ((b >> 7) ^ (b >> 4) ^ (b >> 3) ^ (b >> 0)) & 1;
+	parity |= tmp << 3;
+
+	tmp = ((b >> 6) ^ (b >> 4) ^ (b >> 3) ^
+		(b >> 2) ^ (b >> 1) ^ (b >> 0)) & 1;
+	parity |= tmp << 4;
+
+	return parity;
+}
+
+static uint8_t *mx6ul_nand_fcb_block(struct mx6ul_nand_fcb *fcb)
+{
+	void *block;
+	unsigned size;
+	unsigned version;
+
+	size = nand_writesize + nand_oobsize;
+	block = malloc(size);
+	if (!block) {
+		printf("MX6UL NAND: Unable to allocate FCB block\n");
+		return NULL;
+	}
+	memset(block, 0, size);
+
+	/* Update the FCB checksum */
+	fcb->checksum = mx6ul_nand_block_csum(((uint8_t *)fcb) + 4, 508);
+
+	/* Compute the ECC bytes */
+	version = 3;
+	encode_bch_ecc(fcb, sizeof(*fcb), block, size, version);
+
+	return block;
+}
+
+static int mx6ul_nand_write_fcb(struct mx6ul_nand_fcb *fcb, uint8_t *buf)
+{
+	uint32_t offset;
+	uint8_t *fcbblock;
+	int ret = 0;
+	int i;
+
+	fcbblock = mx6ul_nand_fcb_block(fcb);
+	if (!fcbblock)
+		return -1;
+
+	for (i = 0; i < STRIDE_PAGES * STRIDE_COUNT; i += STRIDE_PAGES) {
+		offset = i * nand_writesize;
+		memcpy(buf + offset, fcbblock, nand_writesize + nand_oobsize);
+		/* Mark the NAND page is OK. */
+		buf[offset + nand_writesize] = 0xff;
+	}
+
+	free(fcbblock);
+
+	return ret;
+}
+
+static int mx6ul_nand_write_dbbt(struct mx6ul_nand_dbbt *dbbt, uint8_t *buf)
+{
+	uint32_t offset;
+	int i = STRIDE_PAGES * STRIDE_COUNT;
+
+	for (; i < 2 * STRIDE_PAGES * STRIDE_COUNT; i += STRIDE_PAGES) {
+		offset = i * nand_writesize;
+		memcpy(buf + offset, dbbt, sizeof(struct mx6ul_nand_dbbt));
+	}
+
+	return 0;
+}
+
+static int mx6ul_nand_write_firmware(struct mx6ul_nand_fcb *fcb, int infd,
+				    uint8_t *buf, size_t fw_size)
+{
+	int ret;
+	unsigned offset1, offset2;
+
+	offset1 = fcb->firmware1_starting_page * nand_writesize;
+	offset2 = fcb->firmware2_starting_page * nand_writesize;
+
+	ret = read(infd, buf + offset1 + FLASH_OFFSET_NAND, fw_size);
+	if (ret != fw_size)
+		return -1;
+
+	memcpy(buf + offset2, buf + offset1 + FLASH_OFFSET_NAND, fw_size);
+
+	return 0;
+}
+
+static void usage(void)
+{
+	printf(
+		"Usage: mx6ulboot [ops] <type> <infile> <outfile>\n"
+		"Augment BootStream file with a proper header for i.MX6UL boot"
+		"\n\n"
+		"  <type>	type of image:\n"
+		"                 \"nand\" for NAND image\n"
+		"                 \"sd\" for SD image\n"
+		"  <infile>     input file, the u-boot.sb bootstream\n"
+		"  <outfile>    output file, the bootable image\n"
+		"\n");
+	printf(
+		"For NAND boot, these options are accepted:\n"
+		"  -w <size>    NAND page size\n"
+		"  -o <size>    NAND OOB size\n"
+		"  -e <size>    NAND erase size\n"
+		"\n"
+		"For SD boot, these options are accepted:\n"
+		"  -p <sector>  Sector where the SGTL partition starts\n"
+	);
+}
+
+static int mx6ul_create_nand_image(int infd, int outfd)
+{
+	struct mx6ul_nand_fcb *fcb;
+	struct mx6ul_nand_dbbt *dbbt;
+	int ret = -1;
+	uint8_t *buf;
+	unsigned img_size;
+	ssize_t wr_size;
+	off_t fw_size;
+	size_t fw_size_aligned;
+
+	img_size = nand_writesize * 512 + 2 * MAX_BOOTSTREAM_SIZE;
+
+	fw_size = lseek(infd, 0, SEEK_END);
+	if (fw_size == -1) {
+		ret = -1;
+		goto err0;
+	}
+	lseek(infd, 0, SEEK_SET);
+	/*
+	 * We have to write one additional page to make the ROM happy.
+	 * Maybe the PagesInFirmware fields are really the number of pages - 1.
+	 * kobs-ng has the same.
+	 */
+	fw_size_aligned = ALIGN(fw_size + nand_writesize, nand_writesize);
+
+	buf = malloc(img_size);
+	if (!buf) {
+		printf("Cannot allocate output buffer of %d bytes\n", img_size);
+		goto err0;
+	}
+	memset(buf, 0, img_size);
+
+	fcb = mx6ul_nand_get_fcb(fw_size_aligned);
+	if (!fcb) {
+		printf("Unable to compile FCB\n");
+		goto err1;
+	}
+
+	dbbt = mx6ul_nand_get_dbbt();
+	if (!dbbt) {
+		printf("Unable to compile DBBT\n");
+		goto err2;
+	}
+
+	ret = mx6ul_nand_write_fcb(fcb, buf);
+	if (ret) {
+		printf("Unable to write FCB to buffer\n");
+		goto err3;
+	}
+
+	ret = mx6ul_nand_write_dbbt(dbbt, buf);
+	if (ret) {
+		printf("Unable to write DBBT to buffer\n");
+		goto err3;
+	}
+
+	ret = mx6ul_nand_write_firmware(fcb, infd, buf, (size_t)fw_size);
+	if (ret) {
+		printf("Unable to write firmware to buffer\n");
+		goto err3;
+	}
+
+	wr_size = write(outfd, buf, img_size);
+	if (wr_size != img_size) {
+		ret = -1;
+		goto err3;
+	}
+
+	ret = 0;
+
+err3:
+	free(dbbt);
+err2:
+	free(fcb);
+err1:
+	free(buf);
+err0:
+	return ret;
+}
+
+static int mx6ul_create_sd_image(int infd, int outfd)
+{
+	int ret = -1;
+	uint32_t *buf;
+	int size;
+	off_t fsize;
+	ssize_t wr_size;
+	struct mx6ul_sd_config_block *cb;
+
+	fsize = lseek(infd, 0, SEEK_END);
+	lseek(infd, 0, SEEK_SET);
+	size = fsize + 4 * 512;
+
+	buf = malloc(size);
+	if (!buf) {
+		printf("Can not allocate output buffer of %d bytes\n", size);
+		goto err0;
+	}
+
+	ret = read(infd, (uint8_t *)buf + 4 * 512, fsize);
+	if (ret != fsize) {
+		ret = -1;
+		goto err1;
+	}
+
+	cb = (struct mx6ul_sd_config_block *)buf;
+
+	cb->signature = cpu_to_le32(0x00112233);
+	cb->primary_boot_tag = cpu_to_le32(0x1);
+	cb->secondary_boot_tag = cpu_to_le32(0x1);
+	cb->num_copies = cpu_to_le32(1);
+	cb->drv_info[0].chip_num = cpu_to_le32(0x0);
+	cb->drv_info[0].drive_type = cpu_to_le32(0x0);
+	cb->drv_info[0].tag = cpu_to_le32(0x1);
+	cb->drv_info[0].first_sector_number = cpu_to_le32(sd_sector + 4);
+	cb->drv_info[0].sector_count = cpu_to_le32((size - 4) / 512);
+
+	wr_size = write(outfd, buf, size);
+	if (wr_size != size) {
+		ret = -1;
+		goto err1;
+	}
+
+	ret = 0;
+
+err1:
+	free(buf);
+err0:
+	return ret;
+}
+
+static int parse_ops(int argc, char **argv)
+{
+	int i;
+	int tmp;
+	char *end;
+	enum param {
+		PARAM_WRITE,
+		PARAM_OOB,
+		PARAM_ERASE,
+		PARAM_PART,
+		PARAM_SD,
+		PARAM_NAND
+	};
+	int type;
+
+	if (argc < 4)
+		return -1;
+
+	for (i = 1; i < argc; i++) {
+		if (!strncmp(argv[i], "-w", 2))
+			type = PARAM_WRITE;
+		else if (!strncmp(argv[i], "-o", 2))
+			type = PARAM_OOB;
+		else if (!strncmp(argv[i], "-e", 2))
+			type = PARAM_ERASE;
+		else if (!strncmp(argv[i], "-p", 2))
+			type = PARAM_PART;
+		else	/* SD/MMC */
+			break;
+
+		tmp = strtol(argv[++i], &end, 10);
+		if (tmp % 2)
+			return -1;
+		if (tmp <= 0)
+			return -1;
+
+		if (type == PARAM_WRITE)
+			nand_writesize = tmp;
+		if (type == PARAM_OOB)
+			nand_oobsize = tmp;
+		if (type == PARAM_ERASE)
+			nand_erasesize = tmp;
+		if (type == PARAM_PART)
+			sd_sector = tmp;
+	}
+
+	if (strcmp(argv[i], "sd") && strcmp(argv[i], "nand"))
+		return -1;
+
+	if (i + 3 != argc)
+		return -1;
+
+	return i;
+}
+
+int main(int argc, char **argv)
+{
+	int infd, outfd;
+	int ret = 0;
+	int offset;
+
+	offset = parse_ops(argc, argv);
+	if (offset < 0) {
+		usage();
+		ret = 1;
+		goto err1;
+	}
+
+	infd = open(argv[offset + 1], O_RDONLY);
+	if (infd < 0) {
+		printf("Input BootStream file can not be opened\n");
+		ret = 2;
+		goto err1;
+	}
+
+	outfd = open(argv[offset + 2], O_CREAT | O_TRUNC | O_WRONLY,
+					S_IRUSR | S_IWUSR);
+	if (outfd < 0) {
+		printf("Output file can not be created\n");
+		ret = 3;
+		goto err2;
+	}
+
+	if (!strcmp(argv[offset], "sd"))
+		ret = mx6ul_create_sd_image(infd, outfd);
+	else if (!strcmp(argv[offset], "nand"))
+		ret = mx6ul_create_nand_image(infd, outfd);
+
+	close(outfd);
+err2:
+	close(infd);
+err1:
+	return ret;
+}
-- 
2.22.0

