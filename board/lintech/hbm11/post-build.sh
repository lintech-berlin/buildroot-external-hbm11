#!/bin/sh

# This script creates u-boot FIT image containing the kernel and the DT.

BOARD_DIR=$(dirname $0)
UBOOT_DIR=$2

# Host utilities
MKIMAGE=$HOST_DIR/bin/mkimage

# kernel.its references zImage and exynos5250-snow.dtb, and all three
# files must be in current directory for mkimage.
echo "Building FIT image"
cp $BOARD_DIR/kernel_fdt.its $BINARIES_DIR/kernel_fdt.its || exit 1
(cd $BINARIES_DIR && $MKIMAGE -f kernel_fdt.its fitImage) || exit 1
rm -f $BINARIES_DIR/kernel_fdt.its

echo "Installing FIT image to $TARGET_DIR/boot"
install -m 0644 -D $BINARIES_DIR/fitImage $TARGET_DIR/boot/fitImage || exit 1

echo "Installing mx6ulboot to $HOST_DIR/bin"
MX6ULBOOT=$HOST_DIR/bin/mx6ulboot
install -m 0755 -D $UBOOT_DIR/tools/mx6ulboot $HOST_DIR/bin/mx6ulboot || exit 1

echo "Creating SPL nand image"
$MX6ULBOOT -o 128 nand $BINARIES_DIR/SPL $BINARIES_DIR/SPL.nand

FIRMWARE_BRCM=$TARGET_DIR/lib/firmware/brcm
if [ -e $FIRMWARE_BRCM ]; then
	echo "Deleting unnecessary binary blobs in $FIRMWARE_BRCM"
	find $FIRMWARE_BRCM \
		-not -name "brcmfmac43430a0-sdio.*" -and \
		-not -name "brcmfmac43430-sdio.*" -and \
		-not -name "brcmfmac4339-sdio.*" -and \
		-not -name "BCM4343A0.hcd" -and \
		-not -name "BCM4335C0.hcd" -and \
		-not -name "BCM4339A0.hcd" \
		-type f -print0 | xargs -0 rm -f
fi

FIRMWARE_IMX_SDMA=$TARGET_DIR/lib/firmware/imx/sdma
if [ -e $FIRMWARE_IMX_SDMA ]; then
	echo "Deleting unnecessary binary blobs in $FIRMWARE_IMX_SDMA"
	find $FIRMWARE_IMX_SDMA \
		-not -name "sdma-imx6q.bin" \
		-type f -print0 | xargs -0 rm -f
fi
