################################################################################
#
# qcacld
#
################################################################################

QCACLD_VERSION = 68f9e028289d41705f4847a626d5e6c421ad239b
QCACLD_SITE = $(call github,TechNexion,qcacld-2.0,$(QCACLD_VERSION))
QCACLD_LICENSE = ISC, BSD-like, GPL-2.0+
QCACLD_LICENSE_FILES = CORE/HDD/src/wlan_hdd_main.c
QCACLD_DEPENDENCIES = linux

QCACLD_MAKE_OPTS = \
	ARCH="$(KERNEL_ARCH)" \
	CROSS_COMPILE="$(TARGET_CROSS)" \
	KERNEL_SRC="$(LINUX_DIR)" \
	CONFIG_CLD_HL_SDIO_CORE=y \
	CONFIG_PER_VDEV_TX_DESC_POOL=1 \
	SAP_AUTH_OFFLOAD=1 \
	CONFIG_QCA_LL_TX_FLOW_CT=1 \
	CONFIG_WLAN_FEATURE_FILS=y \
	CONFIG_FEATURE_COEX_PTA_CONFIG_ENABLE=y \
	CONFIG_QCA_SUPPORT_TXRX_DRIVER_TCP_DEL_ACK=y \
	CONFIG_WLAN_WAPI_MODE_11AC_DISABLE=y \
	TARGET_BUILD_VARIANT=user \
	CONFIG_NON_QC_PLATFORM=y \
	CONFIG_HDD_WLAN_WAIT_TIME=10000

define QCACLD_BUILD_CMDS
	cd $(@D); \
	$(TARGET_MAKE_ENV) $(MAKE) $(QCACLD_MAKE_OPTS) -C $(@D)
endef

define QCACLD_INSTALL_TARGET_CMDS
	cd $(@D); \
	$(TARGET_MAKE_ENV) $(MAKE) $(QCACLD_MAKE_OPTS) \
		INSTALL_MOD_PATH=$(TARGET_DIR) -C $(@D) modules_install
endef

$(eval $(generic-package))
